# Examen Laravel. Mayo 2018

## Base de datos

Sean las siguientes tablas:

- question(#enunciado, a, b, c, d, answer, module_id fk(module))
- exam (#id, date, title, materia_id fk(module), fk(user_id))
- module (#id, code, name)
- exam_question (exam_id, question_id)

### Relaciones

  - N:M exam_question

  - 1:N examm_module

  - 1:N examm_user

  - 1:N module_question

### En el proyecto ya hay:

- Las migraciones y seeders.

- Las rutas necesarias.

### Nó están, es la tarea del alumno:

Modelos, controladores, vistas, política, ...

## Ejercicios

1. 0.5. Pon el proyecto en marcha y logra correr las migraciones y seeders.

1. Mostrar la lista de exámenes (ruta `/exams`).

  - 0.5. Lista en sí.

  - 0.5. Debe aparecer el nombre del usuario propietario y el nombre del módulo.

  - 0.5. Debe aparecer paginado.

  - 0.5. Esta ruta sólo debe ser válida para usuarios logueados, no para invitados.

1. Crea las rutas de API REST siguientes. Debes probarlas con Postman

  - 0.5 Datos de un examen (`/api/exams/{id}`)

  - 0.5 Modifica el anterior para que aparezcan los datos del examen junto a los de su módulo.

  - 0.5 Borra un examen desde Postman. Ruta DELETE `/api/exams/{id}`

2. Mostrar la lista de preguntas (ruta `/questions`)

  - 0.5. Lista en sí.

  - 0.5.  Colorear celda de pregunta correcta. Para eso al TD la clase `"bg-success"`

3. Alta de preguntas (`/modules/create` y POST `/modules`).

  - 0.5. Formulario de alta. Debe mostrarse un `select` para elegir el módulo de la pregunta.

  - 0.5. Alta efectiva de registros.

  - 0.5. Validación:

    - Enunciado, campo `text` requerido y longitud máxima 25

    - Campos a, b, c y d requeridos y longitud máxima 25

    - Campo de `answer`, requerido y longitud máxima 1.

  - 0.5. Muestra de mensajes de la evaluación (inglés válido).


5. Borrado de registros y uso de políticas

  - Crea una política que controle el acceso a los exámenes.

  - 0.5. Borrado de un examen. Añade los botones de borrado en la lista de exámenes.

  - 0.5. Un examen sólo puede ser borrado por su propietario. Añade un enlace que lo pruebe.

  - 0.5. Haz que el enlace anterior sólo pueda ser visible por el propietario.



6. Sesiones

  - 0.5 Añade un enlace a los exámenes ('/exams/{id}/remember') para guardar en sesión un examen.

  - 0.5 Haz que aparezca en la vista de exámenes como "examen recordado: bla, bla". Ojo sólo debe aparecer si existe el elemento en sesión, si no podrías obtener un pantallazo de error.

  - 0.5 Añade un enlace para olvidar el examen recordado.


## Duración del examen 2.5h

## Recordatorio de instalación:

    - Clonar el proyecto

    - Ejecutar `composer install`

    - Copiar el fichero .env: `cp .env.example .env`

    - Crear base de datos y registrar sus parámetros en el `.env`

    - Ejecutar `php artisan key:generate
