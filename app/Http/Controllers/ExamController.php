<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;

class ExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
  {
       $exams = exams::paginate(15);

         $modules = modules::all();
        return view('exams.index', ['exams' => $exams],['modules'=>$modules]);

    }

    public function create()
    {
        $modules = modules::all();
        return view('exams.create', ['modules'=>$modules]);

    }


  public function store(Request $request)
    {

        $reglas = [
            'name'=> 'required|max:255|min:3',
            'module'=> 'required|max:255|min:3'
        ];
        $request->validate($reglas);
        $exams = new exams();
        $exams->fill($request->all());
        $exams->save();
        return redirect('/exams');
    }

    public function show($id)
    {
        $exams = exams::findOrFail($id);
        $modules = modules::all();
        return view('exams.show',['exams'=>$exams],['modules'=>$modules]);

    }

    public function edit($id)
    {
        $exams = exams::findOrFail($id);
          $modules = modules::all();
        return view('exams.edit', ['exams' => $exams],['modules'=>$modules]);
    }

    public function update(Request $request, $id)
    {

        $reglas = [
            'name'=> 'required|max:255|min:3',
            'module'=> 'required|max:255|min:3'
        ];
        $request->validate($reglas);
        $exams = exams::findOrFail($id);
        $exams->fill($request->all());
        $exams->save();
        return redirect('/exams');
    }

    public function destroy($id)
    {

        exams::destroy($id);

        return back();
    }

   

}
