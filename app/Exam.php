<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
   protected $fillable = [
        'name', 'modules'
    ];

    public function Question()
    {
        return $this->belongsTo(\App\Question::class);
    }
}
